import Foundation
import MBProgressHUD
import UIKit

protocol Loadable {
    func startLoading() -> MBProgressHUD
    func stopLoading()
}

extension Loadable where Self: UIViewController {
    func startLoading() -> MBProgressHUD {
        let hud = MBProgressHUD.showAdded(to: view, animated: false)
      //  hud.mode = .annularDeterminate
      //  hud.label.text = "Loading"
        return hud
    }
    
    func stopLoading() {
        MBProgressHUD.hide(for: view, animated: false)
    }
}


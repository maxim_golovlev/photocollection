//
//  Extensions.swift
//  FetchPhotos
//
//  Created by Admin on 28.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import Photos

extension PHAsset {
    
    var originalFilename: String? {
        
        var fname:String?

        let resources = PHAssetResource.assetResources(for: self)
        if let resource = resources.first {
            fname = resource.originalFilename
        }
        
        return fname
    }
    
    var sizeOnDisk: String? {
        
        var sizeOnDisk: String?
        
        let resources = PHAssetResource.assetResources(for: self)
        if let resource = resources.first {
            let unsignedInt64 = resource.value(forKey: "fileSize") as? CLong
            
            let bytes = Int64(bitPattern: UInt64(unsignedInt64!))
            
            let formatter:ByteCountFormatter = ByteCountFormatter()
            formatter.countStyle = .binary
            
            sizeOnDisk = formatter.string(fromByteCount: Int64(bytes))
        }
        
        return sizeOnDisk
    }
}

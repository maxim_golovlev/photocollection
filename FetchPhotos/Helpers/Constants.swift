//
//  Constants.swift
//  FetchPhotos
//
//  Created by Admin on 28.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

enum AppNotifications {
    static let photoFetchingProgress = "photoFetchingProgress"
}

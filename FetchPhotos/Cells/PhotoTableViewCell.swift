//
//  PhotoTableViewCell.swift
//  FetchPhotos
//
//  Created by Admin on 27.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class PhotoTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var _imageView: UIImageView! {
        didSet {
            _imageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var hashLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return 100
    }
    
    func configure(with data:(image: UIImage?, title: String?, size: String?, hash: String?)) {
        _imageView.image = data.image
        titleLabel.text = data.title ?? "???"
        sizeLabel.text = data.size ?? "???"
        hashLabel.text = "hash: " + "\(data.hash ?? "???")"
    }
}

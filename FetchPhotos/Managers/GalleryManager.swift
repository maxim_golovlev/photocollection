//
//  GalleryManager.swift
//  FetchPhotos
//
//  Created by Admin on 27.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import Photos
import PromiseKit

enum ResponseError: Error {
    case withMessage(String?)
}

struct GalleryManager {
    
    private init() {}
    static let shared = GalleryManager()
    
    func fetchAllDevicePhotos(targetSize: CGSize = CGSize.init(width: 100, height: 100)) -> Promise<[LocalPhoto]>{

        return Promise.init(resolvers: { (fullfill, reject) in
            
            checkPhotoPermissionStatus(completion: { (error) in
                if let error = error {
                    reject(ResponseError.withMessage(error))
                } else {
                    self.fetchPhotos(size: targetSize, completion: fullfill)
                }
            })
        })
    }
    
    private func checkPhotoPermissionStatus(completion: @escaping (String?) -> ()) {
        
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized: completion(nil); break
            case .denied, .restricted : completion("Not Allowed"); break
            case .notDetermined: completion("Not determined yet"); break
            }
        }
    }
    
    private func fetchPhotos(size: CGSize, completion: @escaping ([LocalPhoto]) -> ()) {
        
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor.init(key: "creationDate", ascending: true)]
        
        let assets = PHAsset.fetchAssets(with: .image, options: options)
        
        var localPhotos = [LocalPhoto]()
        
        let dispatchGroup = DispatchGroup()
        
        assets.enumerateObjects({ (asset, index, stop) in
            
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.isNetworkAccessAllowed = true
            options.progressHandler = {  (progress, error, stop, info) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotifications.photoFetchingProgress), object: progress)
            }
            
            dispatchGroup.enter()
            
            PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: options, resultHandler: { (image, info) in
                
                localPhotos.append(LocalPhoto(image: image, title: asset.originalFilename, hash: "\(asset.hash)", biteSize: asset.sizeOnDisk))
                dispatchGroup.leave()
            })
        })
        
        dispatchGroup.notify(queue: DispatchQueue.main, execute: {
            completion(localPhotos)
        })
    }
}

//
//  FeedViewController.swift
//  FetchPhotos
//
//  Created by Admin on 27.11.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

protocol FeedViewProtocol: BaseView {
    func photosUploaded(photos: [LocalPhoto])
}

class FeedViewController: UIViewController {
  
  // MARK: - Public properties
  
  lazy var presenter:FeedPresenterProtocol = FeedPresenter(view: self)
  
  // MARK: - Private properties
  
    private var tableDirector: TableDirector!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableDirector = TableDirector.init(tableView: tableView)
            tableView.tableFooterView = UIView()
            tableView.allowsSelection = false
        }
    }
    @IBOutlet weak var getPhotosButton: UIButton!
    // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  // MARK: - Display logic
  
  // MARK: - Actions
    
    @IBAction func renderPhotos(_ sender: Any) {
        
        presenter.fetchDevicePhotos()
    }
  
  // MARK: - Overrides
    
  // MARK: - Private functions
    
    func showTableView() {
        
        tableView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.tableView.transform = .identity
            self.tableView.isHidden = false
            self.getPhotosButton.isHidden = true
            
        })
    }
}

extension FeedViewController:  FeedViewProtocol {
    func photosUploaded(photos: [LocalPhoto]) {
        
        showTableView()
        
        tableDirector.clear()
        
        let section = TableSection()
        let rows = photos.flatMap({ TableRow<PhotoTableViewCell>.init(item: (image: $0.image, title: $0.title, size: $0.biteSize, hash: $0.hash)) })
        
        section.append(rows: rows)
        tableDirector.append(section: section)
        
        tableDirector.reload()
    }
    
    func downloadProgress(progress: Int) {
        
        
    }
}

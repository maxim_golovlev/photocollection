//
//  FeedPresenter.swift
//  FetchPhotos
//
//  Created by Admin on 27.11.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol FeedPresenterProtocol: class {
    weak var view:FeedViewProtocol? { get set }
    func fetchDevicePhotos()
}

class FeedPresenter {
  
  // MARK: - Public variables
  weak var view:FeedViewProtocol?
  
  // MARK: - Private variables
    
    private var hud: MBProgressHUD?
  
  // MARK: - Initialization
  init(view:FeedViewProtocol) {
    self.view = view
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.updateProgressHud(_:)), name: NSNotification.Name(rawValue: AppNotifications.photoFetchingProgress), object: nil)
    
  }
    @objc private func updateProgressHud(_ notification: NSNotification) {
        if let progress = notification.object as? Double {
            hud?.progress = Float(progress)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension FeedPresenter: FeedPresenterProtocol {
  
    func fetchDevicePhotos(){
        
        hud = self.view?.startLoading()
        
        GalleryManager.shared.fetchAllDevicePhotos()
            .then { (photos) -> Void in
                self.view?.photosUploaded(photos: photos)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }
    }
}

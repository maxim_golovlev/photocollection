//
//  LocalPhoto.swift
//  FetchPhotos
//
//  Created by Admin on 27.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import Photos

struct LocalPhoto {
    let image: UIImage?
    let title: String?
    let hash: String?
    let biteSize: String?
}
